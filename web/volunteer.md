---
layout: page
title: "Volunteer"
permalink: /volunteer/
---

# LAS 2021 Volunteers Wanted

The Linux App Summit (LAS) is designed to accelerate the growth of the Linux application ecosystem by bringing together everyone involved in creating a great Linux application user experience. This event is organized by GNOME and KDE.

The organizers of the LAS are pleased to invite all enthusiastic and motivated contributors to help us run the conference and make it a memorable experience for everyone.

Volunteers duties: 

 * Workshop assistant: Assist the workshop virtual rooms with any requests the speakers might have and also give reminders when the time is up!

 * Moderator/speaker's assistant: Act as host for one of the talk rooms. Introduce the speakers, keep track of the time and signal to speakers if they're running over. 

 * Social media manager: Keeping our social media updated with announcements: such as upcoming talks,workshops, keynote etc.

If you want to help or have any further questions you can contact us at: <a href="mailto:appsummit@lists.freedesktop.org">appsummit@lists.freedesktop.org</a>
