---
layout: page
permalink: /coc/
toc: false
---

# LAS Code of Conduct

## Our Pledge

In the interest of fostering an open and welcoming environment, the Linux
App Summit (LAS) team pledges to making participation in our event and
our community a harassment-free experience for everyone, regardless of age, body
size, disability, ethnicity, sex characteristics, gender identity and expression,
level of experience, education, socio-economic status, nationality, personal
appearance, race, religion, or sexual identity and orientation.

## Our Standards

Examples of behavior that contributes to creating a positive environment
include:

* Using welcoming and inclusive language
* Being respectful of differing viewpoints and experiences
* Gracefully accepting constructive criticism
* Focusing on what is best for the community
* Showing empathy towards other community members
* Avoiding presumptions about other people

Examples of unacceptable behavior by participants include:

* The use of violent or sexualized language or imagery in public spaces (including
* presentation slides)   
* Unwelcome physical contact, sexual attention or advances
* Deliberate indimidation, stalking, or following   
* Sustained disruption of talks or other events   
* Trolling, insulting/derogatory comments, and personal or political attacks
* Public or private harassment
* Publishing others' private information, such as a physical or electronic
  address, without explicit permission
* Other conduct which could reasonably be considered inappropriate in a
  professional setting
* Possession of an offensive weapon (including anything deemed to be a weapon by
  the orgainzers)

## Our Responsibilities

Event organizers are responsible for clarifying the standards of acceptable
behavior and are expected to take appropriate and fair corrective action in
response to any instances of unacceptable behavior.

Event organizers have the right and responsibility to ban temporarily or
permanently any participant for behaviors that they deem inappropriate,
threatening, offensive, or harmful.

## Scope

This Code of Conduct applies within all LAS event spaces, 
and it also applies when
an individual is representing the LAS event or community in public spaces.

## Enforcement

Instances of abusive, harassing, or otherwise unacceptable behavior may be
reported by contacting the LAS team at appsummit@lists.freedesktop.org. Please 
see detailed infromation about how to request help [here.](http://linuxappsummit.org/coc/report)

All complaints will be reviewed and investigated and will result in a response 
that is deemed necessary and appropriate to the circumstances. The LAS team 
is obligated to maintain confidentiality with regard to the reporter of an 
incident.

LAS participants who do not follow or enforce the Code of Conduct in good
faith may face temporary or permanent repercussions as determined by other
members of the event's leadership.

## Attribution

This Code of Conduct is adapted from the [Contributor Covenant](https://www.contributor-covenant.org/)
, version 1.4, available at https://www.contributor-covenant.org/version/1/4/code-of-conduct.html,
and the [GNOME Conference Code of Conduct](https://wiki.gnome.org/CodeOfConduct/Events)


For answers to common questions about this code of conduct, see
https://www.contributor-covenant.org/faq
